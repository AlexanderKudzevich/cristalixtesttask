package me.swaggy.arenatest.Listeners;

import me.swaggy.arenatest.Arena;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerListener implements Listener {

  @EventHandler
  public void PlayerKillEvent(PlayerDeathEvent e) {
    Player player = e.getEntity();
    Player killer = player.getKiller();

    Arena arena = Arena.getInstance();
    if (arena.isPlayerInArena(killer) && arena.isPlayerInArena(player)) {
      arena.addKillToPlayer(killer); // Kill's counter +1
      Bukkit.broadcastMessage(
          ChatColor.RED + killer.getName() + " уже имеет " + arena.getPlayerKills(killer)
              + " убийств!");
      arena.giveLootToPlayer(killer); // Giving reward
    }
  }

  @EventHandler
  public void PlayerDamagedPlayerEvent(EntityDamageByEntityEvent e) {
    if ((e.getEntity() instanceof Player) && (e.getDamager() instanceof Player)) {
      Player player = (Player) e.getEntity();
      Player damager = (Player) e.getDamager();

      //
      Arena arena = Arena.getInstance();
      if ((!arena.isPlayerInArena(player)) || (
          !arena.isPlayerInArena(damager))) {
        e.setCancelled(true);
      }
    }
  }
}
