package me.swaggy.arenatest.Commands;

import me.swaggy.arenatest.Arena;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RestCommand implements CommandExecutor {

  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Player player = (Player) sender;

    if (!Arena.getInstance().isPlayerInArena(player)) {
      player.sendMessage(ChatColor.RED + "Вы не находитесь на арене!");
    } else {
      Arena.getInstance().removePlayerFromArena(player);
      player.sendMessage(ChatColor.RED + "Вы решили отдохнуть!");
      //TODO: TELEPORT PLAYER TO SAFE ZONE
    }
    return true;
  }
}
