package me.swaggy.arenatest.Commands;

import me.swaggy.arenatest.Arena;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayCommand implements CommandExecutor {

  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Player player = (Player) sender;

    if (Arena.getInstance().isPlayerInArena(player)) {
      player.sendMessage(ChatColor.RED + "Вы уже на арене!");
    } else {
      Arena.getInstance().addPlayerToArena(player);
      player.sendMessage(ChatColor.RED + "Добро пожаловать на арену!");
      //TODO: TELEPORT TO ARENA
    }

    return true;
  }
}
