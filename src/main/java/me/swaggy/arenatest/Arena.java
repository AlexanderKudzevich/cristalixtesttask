package me.swaggy.arenatest;

import java.util.HashMap;
import java.util.Map;
import me.swaggy.arenatest.Utils.RandomItem;
import org.bukkit.entity.Player;

public class Arena {

  public static Arena getInstance() {
    if (instance == null) {
      instance = new Arena();
    }

    return instance;
  }

  private static Arena instance;

  private Map<Player, Integer> arenaPlayers;

  private Arena() {
    arenaPlayers = new HashMap<Player, Integer>();
  }

  public int getPlayerKills(Player player) {
    return arenaPlayers.get(player);
  }

  public void addKillToPlayer(Player player) {
    Integer count = arenaPlayers.get(player);
    if (count != null) {
      arenaPlayers.put(player, count + 1);
    }
  }

  public void giveLootToPlayer(Player player) {
    player.getInventory().addItem(RandomItem.GetRandomItem());
  }

  public boolean isPlayerInArena(Player player) {
    return arenaPlayers.containsKey(player);
  }

  public void addPlayerToArena(Player player) {
    arenaPlayers.put(player, 0);
  }
  public void removePlayerFromArena(Player player) {
    arenaPlayers.remove(player);
  }
}
