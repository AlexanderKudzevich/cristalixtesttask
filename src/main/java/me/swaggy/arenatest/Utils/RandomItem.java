package me.swaggy.arenatest.Utils;

import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class RandomItem {

  private static ArrayList<ItemStack> randomItems = new ArrayList<ItemStack>();

  public static void Initialize() {
    //TODO: From file(config)
    randomItems.add(new ItemStack(Material.GOLDEN_APPLE, 1));
    randomItems.add(new ItemStack(Material.DIAMOND_SWORD, 1));
    randomItems.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
    randomItems.add(new ItemStack(Material.SKULL_ITEM, 1, (short) 4)); // Creeper's Head
  }

  public static ItemStack GetRandomItem() {
    Random random = new Random();
    return randomItems.get(random.nextInt(randomItems.size()));
  }
}
