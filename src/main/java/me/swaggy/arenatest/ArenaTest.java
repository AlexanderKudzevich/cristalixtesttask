/*написать небольшую и несложную арену, подсчитыввающую количество убийств,
    выдающую за каждое убийство случайный предмет из произвольного списка (например, золотое яблоко, алмазный меч, кольчужный нагрудник и голову крипера)
    и с небольшой зоной, где отключен пвп, на которую можно телепортироваться, введя команду /rest.*/


package me.swaggy.arenatest;

import me.swaggy.arenatest.Commands.PlayCommand;
import me.swaggy.arenatest.Commands.RestCommand;
import me.swaggy.arenatest.Listeners.PlayerListener;
import me.swaggy.arenatest.Utils.RandomItem;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;


public class ArenaTest extends JavaPlugin {

  @Override
  public void onEnable() {
    Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

    this.getCommand("play").setExecutor(new PlayCommand());
    this.getCommand("rest").setExecutor(new RestCommand());

    RandomItem.Initialize();
  }

}
